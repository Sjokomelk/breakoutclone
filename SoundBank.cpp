#include "StdAfx.h"
#include "SoundBank.h"


SoundBank::SoundBank()
{
}


SoundBank::~SoundBank()
{
}


void SoundBank::Init()
{
	m_fVolume = 50.0f;
	
	// Load all the sound effects
	Load("BlockDestroyed");
	Load("BlockTouched");
	Load("Death");
	Load("PadTouched");

	Load("MenuNavigate");
	Load("MenuSelect");
	Load("EnterMenu");
}

void SoundBank::Clean() {
	// Stop all sounds
	StopSounds();

	// Delete all loaded sounds
	std::map<const std::string, sf::SoundBuffer*>::iterator it;

    for (it = m_xSoundBuffers.begin(); it != m_xSoundBuffers.end(); it++) {
        delete it->second;
    }
}

void SoundBank::PlayStatic(std::string id) {
	// Whenever play is called, remove outdated sounds
	RemoveOutdatedSounds();

	if (m_xSoundBuffers.find(id) != m_xSoundBuffers.end()) {
		// Push back new sound into the sounds vector
		int i = m_xSounds.size();
		sf::Sound* tempSound = new sf::Sound;

		// Play the sound
		tempSound->setBuffer(*m_xSoundBuffers[id]);
		tempSound->setVolume(m_fVolume);

		tempSound->play();
		m_xSounds.push_back(tempSound);
	} else {
		LOG("ERROR! Could not find the sound: Assets/Sound/" + id + ".ogg");
	}
}
void SoundBank::PlayDynamic(std::string id) {
	// Whenever play is called, remove outdated sounds
	RemoveOutdatedSounds();

	if (m_xSoundBuffers.find(id) != m_xSoundBuffers.end()) {
		// Push back new sound into the sounds vector
		int i = m_xSounds.size();
		sf::Sound* tempSound = new sf::Sound;

		// Play the sound
		tempSound->setBuffer(*m_xSoundBuffers[id]);
		
		// Apply slight variations Pitch +- 5 and Volume +- 4
		tempSound->setPitch(0.95f + (rand()%10)/100.0f);
		if (m_fVolume > 96.0f) {
			tempSound->setVolume(92.0f + (rand()%80)/10.0f);
		} else if (m_fVolume < 4.0f) {
			tempSound->setVolume((rand()%80)/10.0f);
		} else {
			tempSound->setVolume(m_fVolume - 4.0f + (rand()%80)/10.0f);
		}

		tempSound->play();
		m_xSounds.push_back(tempSound);
	} else {
		LOG("ERROR! Could not find the sound: Assets/Sound/" + id + ".ogg");
	}
}
void SoundBank::StopSounds() {
	// Stop all sounds
	for (size_t i=0; i<m_xSounds.size(); i++) {
		delete m_xSounds[i];
	}
	m_xSounds.clear();
}
void SoundBank::SetVolume(float volume) {
	if (volume <= 0) {
		m_fVolume = 0;
	} else if (volume > 100.0f) {
		m_fVolume = 100.0f;
	} else {
		m_fVolume = volume;
	}
}
void SoundBank::ChangeVolume(float steps) {
	SetVolume(m_fVolume + steps);
}
float SoundBank::GetVolume() {
	return m_fVolume;
}
void SoundBank::Load(std::string id) {
	sf::SoundBuffer* temp = new sf::SoundBuffer;
	if (!(temp->loadFromFile("Assets/Sound/" + id + ".ogg"))){
		LOG("Assets/Sound/" << id << ".ogg did not load");
	}
	m_xSoundBuffers.insert(std::pair<const std::string, sf::SoundBuffer*>(id, temp));
}
void SoundBank::RemoveOutdatedSounds() {
	for (unsigned int i=0; i<m_xSounds.size(); i++) {
		if (m_xSounds[i]->getStatus() == sf::Sound::Stopped) {
			delete m_xSounds[i];
			m_xSounds.erase(m_xSounds.begin() + i);

			// Don't count up after an erase
			i--;
		}
	}
}
