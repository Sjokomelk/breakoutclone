#include <SFML/Graphics.hpp>
#include <vector>

#ifndef IMAGEBANK_H
#define IMAGEBANK_H


class ImageBank
{
public:

	ImageBank();
	~ImageBank();

	void Init();
	void Clean();

	sf::Texture& GetImage(std::string id);
	sf::Font& GetFont();
	
private:
	void Load(std::string id);

	std::map<const std::string, sf::Texture> m_xTextures;
	sf::Font m_Font;
};
#endif