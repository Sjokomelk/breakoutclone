#include "StdAfx.h"
#include "BasicBlock.h"


BasicBlock::BasicBlock()
{
}


BasicBlock::~BasicBlock()
{
}

void BasicBlock::Init()
{

}

void BasicBlock::Update(float delta)
{
	auto it = m_Components.begin();
	while (it != m_Components.end())
	{
		(*it)->Update(delta);
		it++;
	}
}

void BasicBlock::Clean()
{

}

