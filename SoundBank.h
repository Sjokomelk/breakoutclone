#pragma once

class SoundBank
{
public:
	SoundBank();
	~SoundBank();

	void Init();
	void Clean();

	void PlayStatic(std::string id);
	void PlayDynamic(std::string id);
	void StopSounds();
	void SetVolume(float volume);
	void ChangeVolume(float deltaVolume);
	float GetVolume();

private:
	void Load(std::string id);
	void RemoveOutdatedSounds();

	std::map<const std::string, sf::SoundBuffer*> m_xSoundBuffers;
	std::vector<sf::Sound*> m_xSounds;
		
	float m_fVolume;
};

