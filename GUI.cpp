#include "StdAfx.h"
#include "GUI.h"
#include <sstream>


GUI::GUI()
{
}


GUI::~GUI()
{
}

void GUI::Init(sf::RenderWindow* window, GameObject* player, ImageBank* imagebank, int numLevels)
{
	m_Window = window;
	m_ImageBank = imagebank;
	m_Points = 0;
	m_NumLevels = numLevels;

	m_Player = static_cast<Player*>(player);

	sf::Color color(100,20,130);

	m_Score.setFont			(m_ImageBank->GetFont());
	m_Score.setCharacterSize(35);
	m_Score.setColor		(color);
	m_Score.setPosition		(20.0f, 865.0f);

	m_Lives.setFont			(m_ImageBank->GetFont());
	m_Lives.setCharacterSize(35);
	m_Lives.setColor		(color);
	m_Lives.setPosition		(1050.0f, 865.0f);

	m_LevelsInfo.setFont			(m_ImageBank->GetFont());
	m_LevelsInfo.setCharacterSize	(35);
	m_LevelsInfo.setColor			(color);
	m_LevelsInfo.setPosition		(525.0f, 865.0f);

}

void GUI::Clean()
{

}

void GUI::IncreaseScore(int score)
{
	m_Points += score;
}


void GUI::Update(float delta)
{
	{
		std::stringstream ss;

		ss << "Score: " << m_Points;
		std::string points = ss.str();
		m_Score.setString(points);
	}
	{
		std::stringstream ss;
		ss << "Lives: " << m_Player->GetComponent<HealthComponent>("HealthComponent")->GetHealth();
		m_Lives.setString(ss.str());
	}


	m_Window->draw(m_Score);
	m_Window->draw(m_Lives);
	m_Window->draw(m_LevelsInfo);
}

void GUI::SetCurrentLevel(int level) {

	std::stringstream ss;
	ss << "Level: " << level+1 << "/" << m_NumLevels;
	m_LevelsInfo.setString(ss.str());
}
