#include "StdAfx.h"
#include "ContactListener.h"


ContactListener::ContactListener()
{
}


ContactListener::~ContactListener()
{
}


void ContactListener::SetGameObjectManager(GameObjectManager* GO_Manager)
{
	m_GO_Manager = GO_Manager;
}

void ContactListener::BeginContact(b2Contact* contact)
{
	void* bodyUserDataA = contact->GetFixtureA()->GetBody()->GetUserData();
	void* bodyUserDataB = contact->GetFixtureB()->GetBody()->GetUserData();

	if (bodyUserDataA == nullptr || bodyUserDataB == nullptr) return;
	
	// needed a gameobject, otherwise the casting of the datatypes makes it crash.
	// userdata for the objects you compare must have at least the same base class
	// which is what makes this a bit weird.
	/*
	Messy code, however now it's just a matter of getting it done. 
	Elegant solutions out the window...
	*/
	
	if ( static_cast<Ball*>( bodyUserDataA )->IsType("Ball") && static_cast<Background*>(bodyUserDataB)->IsType("Background"))
	{	
		Ball* ball = (Ball*)bodyUserDataA;
		SoundComponent* sound = ball->GetComponent<SoundComponent>("SoundComponent");
		LOG("<===> Wall is A || Ball is B");
		SoundBank* noise = ball->GetComponent<SoundComponent>("SoundComponent")->GetSoundBank();
		noise->PlayDynamic("PadTouched");
		//sound->PlaySound();
	} else if (static_cast<Ball*>( bodyUserDataB )->IsType("Ball") && static_cast<Background*>(bodyUserDataA)->IsType("Background"))
	{
		Ball* ball = (Ball*)bodyUserDataB;
		SoundComponent* sound = ball->GetComponent<SoundComponent>("SoundComponent");
		//sound->PlaySound();
		LOG("<===> Ball is B || Wall is A");
		SoundBank* noise = ball->GetComponent<SoundComponent>("SoundComponent")->GetSoundBank();
		noise->PlayDynamic("PadTouched");
	}
	
	if (static_cast<Ball*> (bodyUserDataA)->IsType("Ball") && static_cast<Block*>(bodyUserDataB)->IsType("Block"))
	{
		LOG("Ball is A: " << " block is B");
		SoundBank* noise = static_cast<Ball*>(bodyUserDataA)->GetComponent<SoundComponent>("SoundComponent")->GetSoundBank();
		noise->PlayDynamic("PadTouched");

		Block* gameobject = static_cast<Block*>(bodyUserDataB);
		
		HealthComponent* health = gameobject->GetComponent<HealthComponent>("HealthComponent");
		health->DecreaseHealth();
		LOG ("Health: " << health->GetHealth());

	} else if (static_cast<Ball*> (bodyUserDataB)->IsType("Ball") && static_cast<Block*>(bodyUserDataA)->IsType("Block"))
	{
		LOG("Ball is B: " << " block is A");
		SoundBank* noise = static_cast<Ball*>(bodyUserDataB)->GetComponent<SoundComponent>("SoundComponent")->GetSoundBank();
		noise->PlayDynamic("PadTouched");
		
		Block* gameobject = static_cast<Block*>(bodyUserDataA);
		HealthComponent* health = gameobject->GetComponent<HealthComponent>("HealthComponent");
		health->DecreaseHealth();
		LOG ("Health: " << health->GetHealth());
	}

	/*
	The below doesn't work as well as I had hoped, however it works better than nothing
	you have at least some control.

	It works a bit weird, I haven't really taken the time to figure out what happens
	I looked at how we did it on Fly or Die, with some minor modifications.

	*/

	if (static_cast<Ball*> (bodyUserDataA)->IsType("Ball") && static_cast<Player*>(bodyUserDataB)->IsType("Player"))
	{
		BallHitPad((Ball*)bodyUserDataA, ((Player*)bodyUserDataB)->GetComponent<CollisionComponent>("CollisionComponent")->GetBody()->GetPosition().x * PPM);

	} else if (static_cast<Ball*> (bodyUserDataB)->IsType("Ball") && static_cast<Player*>(bodyUserDataA)->IsType("Player"))
	{
		BallHitPad((Ball*)bodyUserDataB, ((Player*)bodyUserDataA)->GetComponent<CollisionComponent>("CollisionComponent")->GetBody()->GetPosition().x * PPM);
	}
}

void ContactListener::EndContact(b2Contact* contact)
{

}

void ContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
	//void* bodyUserData = contact->GetFixtureA->GetBody()->GetUserData();

}

void ContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
	//void* bodyUserData = contact->GetFixtureA();


	//bodyUserData = contact->GetFixtureB();
		
}
void ContactListener::BallHitPad(GameObject* ball, float padPosX) {
	SoundBank* noise = static_cast<Ball*>(ball)->GetComponent<SoundComponent>("SoundComponent")->GetSoundBank();
	noise->PlayDynamic("PadTouched");

	b2Body* body = ball->GetComponent<CollisionComponent>("CollisionComponent")->GetBody();

	if (body->GetPosition().x * PPM > padPosX + 20.0f) {
		body->ApplyLinearImpulse(b2Vec2(4.0f, 0.0f), body->GetWorldCenter());
	} else if (body->GetPosition().x * PPM < padPosX - 20.0f) {
		body->ApplyLinearImpulse(b2Vec2(-4.0f, 0.0f), body->GetWorldCenter());
	}
}