#include "StdAfx.h"
#include "Game.h"

Game::Game()
{
}


Game::~Game()
{
}

void Game::Init()
{
	m_SoundBank = new SoundBank();
	m_SoundBank->Init();

	m_ImageBank = new ImageBank();
	m_ImageBank->Init();

	m_Window = new sf::RenderWindow(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Breakout Clone", sf::Style::Default);
	m_Window->setFramerateLimit(60);

	m_fDelta = m_Clock.restart().asSeconds();

	MenuState* menuState = new MenuState();
	GameState* gameState = new GameState();
	LevelEditorState* editorState = new LevelEditorState;
	LoseState* loseState = new LoseState;
	WinState* winState = new WinState;

	menuState->Init(m_Window, m_ImageBank, m_SoundBank);
	gameState->Init(m_Window, m_ImageBank, m_SoundBank);
	editorState->Init(m_Window, m_ImageBank, m_SoundBank);
	loseState->Init(m_Window, m_ImageBank, m_SoundBank);
	winState->Init(m_Window, m_ImageBank, m_SoundBank);

	m_States.push_back(menuState);
	m_States.push_back(gameState);
	m_States.push_back(editorState);
	m_States.push_back(loseState);
	m_States.push_back(winState);

	m_CurrentState = State::MENUSTATE;
	m_States[m_CurrentState]->Enter();
}

void Game::Update()
{

	while (m_Window->isOpen())
	{
		m_fDelta = m_Clock.restart().asSeconds();
		while (m_Window->pollEvent(m_Event))
		{
			if (m_Event.type == sf::Event::Closed)
			{
				m_Window->close();
			}
		}

		m_Window->clear();
		State::StateType stateReturn = m_States[m_CurrentState]->Update(m_fDelta);

		if (stateReturn == State::EXITGAME) {
			m_Window->close();
		} else if (stateReturn != m_CurrentState) {
			m_CurrentState = stateReturn;
			m_States[m_CurrentState]->Enter();
		}

		m_Window->display();
	}
}

void Game::Clean() 
{
	for (unsigned int i = 0; i < m_States.size(); i++)
	{
		m_States[i]->Clean();
		delete m_States[i];
	}
	m_States.clear();

	m_SoundBank->Clean();
	m_ImageBank->Clean();
	delete m_SoundBank;
	delete m_ImageBank;
	delete m_Window;
}