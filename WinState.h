#pragma once
#include "state.h"
class WinState : public State
{
public:
	WinState();
	~WinState();

	void Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank);
	void Enter();
	StateType Update(float delta);
	void Clean();

private:

	sf::Sprite m_Display;
};

