#pragma once
#include "state.h"
#include "Level.h"

class LevelEditorState : public State
{
public:
	LevelEditorState();
	~LevelEditorState();

	void Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank);
	void Enter();
	StateType Update(float delta);
	void Clean() {} // Empty

private:
	void Draw();

	void ReadFromFile();
	void WriteToFile();
	void LoadSelectedLevel();

	void SetStatusText();

	unsigned int m_SelectedLevel;
	Level m_LevelInProgress;
	std::vector<Level> m_Levels;

	unsigned int m_SelectedX;
	unsigned int m_SelectedY;

	sf::Sprite m_Marker;
	sf::Sprite m_BasicBlock;
	sf::Sprite m_StrongBlock;
	sf::Sprite m_IndestructableBlock;

	sf::Text m_StatusText;
	sf::Text m_InfoText;
};

