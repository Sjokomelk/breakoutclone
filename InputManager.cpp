#include "StdAfx.h"
#include "InputManager.h"


InputManager::InputManager()
{

	
}


InputManager::~InputManager()
{
}

bool InputManager::IsKeyDown(sf::Keyboard::Key key)
{
	return m_KeyDown[key];
}

bool InputManager::IsKeyUp(sf::Keyboard::Key key)
{
	if (m_KeyDown[key] == InputManager::m_KeyUp[key] ) return false;
	else if (m_KeyDown[key] == false && m_KeyDown[key] == true) return true;
	return false;
}

bool InputManager::RegisterFirstKeyPress(sf::Keyboard::Key key)
{
	if (m_KeyDown[key] == InputManager::m_KeyUp[key]){ return false;}
	else if (m_KeyDown[key] == true && InputManager::m_KeyUp[key] == false) return true;
	else return false;
}

void InputManager::Update()
{

}
