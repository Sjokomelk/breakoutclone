#include "ImageBank.h"
#include "stdafx.h"

/*
FlyWeight
*/

ImageBank::ImageBank() {}
ImageBank::~ImageBank() {}

void ImageBank::Init() {
	Load("BlockMarker");
	Load("BasicBlock");
	Load("StrongBlock");
	Load("IndestructableBlock");
	Load("Background");
	Load("PlayerPad");
	Load("Ball");

	Load("LoseState");
	Load("WinState");

	// Load the font
	if (!m_Font.loadFromFile("Assets/Graphics/arial.ttf"))
	{
		// Error...
		LOG("Error: Assets/Graphics/arial.ttf did not load");
	}
}
void ImageBank::Clean() {
	
}

sf::Texture& ImageBank::GetImage(std::string id) {
	if (m_xTextures.find(id) == m_xTextures.end()) {
		LOG("Could not find " << id << ".png in imagebank.");
	}

	return m_xTextures[id];
}
sf::Font& ImageBank::GetFont() {
	return m_Font;
}

void ImageBank::Load(std::string id) {
	sf::Texture image;
	m_xTextures.insert(std::pair<const std::string, sf::Texture>(id, image));

	if (!(m_xTextures[id].loadFromFile("Assets/Graphics/" + id + ".png"))){
		LOG("Assets/Graphics/" << id << ".png did not load!");
	}
}