#include "StdAfx.h"
#include "Background.h"


Background::Background()
{
}


Background::~Background()
{

}

void Background::Init()
{

}

void Background::Update(float delta)
{
	auto it = m_Components.begin();
	while (it != m_Components.end())
	{
		(*it)->Update(delta);
		it++;
	}
}

void Background::Clean()
{

}

bool Background::IsType(const std::string &type)
{
	return type.compare("Background") == 0;
}


