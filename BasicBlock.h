#pragma once
#include "block.h"

class BasicBlock : public GameObject
{
public:
	BasicBlock();
	~BasicBlock();

	void Init();
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type) {return type.compare("Block") == 0;}

private:

};

