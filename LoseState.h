#pragma once
#include "state.h"
class LoseState : public State
{
public:
	LoseState();
	~LoseState();

	void Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank);
	void Enter();
	StateType Update(float delta);
	void Clean();

private:
	float m_DisplayTime;
	sf::Sprite m_Display;
};

