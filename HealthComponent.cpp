#include "StdAfx.h"
#include "HealthComponent.h"


HealthComponent::HealthComponent()
{
	m_Health = 5;
	m_Destructable = true;
}


HealthComponent::~HealthComponent()
{
}

bool HealthComponent::IsType(const std::string &type)
{
	return type.compare("HealthComponent") == 0;
}

void HealthComponent::Init(GameObject* gameobject)
{
	m_GameObject = gameobject;
}

void HealthComponent::Update(float delta)
{
	
}

void HealthComponent::Clean()
{

}

void HealthComponent::SetDestructable(bool destructable)
{
	m_Destructable = destructable;
}

bool HealthComponent::IsDestructable()
{
	return m_Destructable;
}

void HealthComponent::SetHealth(int health)
{
	m_Health = health;
	LOG("Setting health to: " << health);
}

int HealthComponent::GetHealth()
{
	return m_Health;
}

void HealthComponent::DecreaseHealth()
{
	if (m_Destructable == true)
	{
		m_Health--;

		if ((m_Health == 1) && (m_GameObject->IsType("Block"))) {
			m_GameObject->GetComponent<SpriteComponent>("SpriteComponent")->LoadSprite("BasicBlock");
			m_GameObject->GetComponent<SpriteComponent>("SpriteComponent")->SetOriginCenter();
			m_GameObject->GetComponent<SpriteComponent>("SpriteComponent")->SetPosition();
		}
	}
	
}

// didn't work;
// HealthComponent& HealthComponent::operator--()
//{
//	m_Health -= 1;
//	return *this;
//}
//
// HealthComponent HealthComponent::operator--(int)
// {
//	 m_Health -=1;
//	 return *this;
// }