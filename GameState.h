#pragma once
class Player;
class GameObjectManager;
class Background;
class GameObjectFactory;
class ContactListener;

#include "GUI.h"
#include "Level.h"

#define DEGTORAD 0.0174532925199432957f

class GameState : public State
{
public:
	GameState();
	~GameState();

	void Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank);
	void Enter();
	StateType Update(float delta);
	void Clean();
	void CleanWorld();

	void CreatePlayer();
	void CreateBlock(sf::Vector2f position, Level::BlockType type);
	void CreateBackground();
	void CreateWalls();
	void CreateBall();

	void ReadLevels();
	bool OpenLevel();
	
private:
	
	GameObjectManager* m_ObjectManager;
	b2World* m_World;

	GameObjectFactory* m_GO_Factory;
	
	std::vector<Level> m_Levels;
	unsigned int m_CurrentLevel;

	GUI m_GUI;

	// Collision data / contacts
	ContactListener* m_Contacts;
	GameObject* m_WallData;
	std::vector<b2Body*> m_Blocks;
	GameObject* m_Ball;
	GameObject* m_Player;

	sf::Text m_InfoText;
	bool m_DrawInfo;
};

