#pragma once
class InputManager
{
public:
	InputManager();
	~InputManager();

	bool IsKeyDown(sf::Keyboard::Key key);
	bool IsKeyUp(sf::Keyboard::Key key);
	bool RegisterFirstKeyPress(sf::Keyboard::Key key);
	void Update();

//private:
	bool* m_KeyDown;// [sf::Keyboard::KeyCount];
	bool* m_KeyUp;//[sf::Keyboard::KeyCount];

};

