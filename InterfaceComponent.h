#pragma once
class GameObject;
class InterfaceComponent
{
public:
	InterfaceComponent() {};
	virtual ~InterfaceComponent() {};

	virtual void Init(GameObject* gameobject) = 0;
	virtual void Update(float delta) = 0;
	virtual void Clean() = 0;
	
	virtual bool IsType(const std::string &type) = 0;
	
private:

};