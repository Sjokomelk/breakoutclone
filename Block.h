#pragma once
#include "gameobject.h"
class Block : public GameObject
{
public:
	Block();
	virtual ~Block();

	virtual void Init() = 0;
	virtual void Update() = 0;
	virtual void Clean() = 0;

	bool IsType(const std::string &type) {return type.compare("Block") == 0;}

protected:


};

