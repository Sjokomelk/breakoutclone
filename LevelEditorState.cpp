#include "StdAfx.h"
#include "state.h"
#include "LevelEditorState.h"


LevelEditorState::LevelEditorState()
{
}


LevelEditorState::~LevelEditorState()
{
}

void LevelEditorState::Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank)
{
	State::Init(window, imageBank, soundBank);
	
	// Set sprites
	m_Marker.setTexture(m_ImageBank->GetImage("BlockMarker"));
	m_BasicBlock.setTexture(m_ImageBank->GetImage("BasicBlock"));
	m_StrongBlock.setTexture(m_ImageBank->GetImage("StrongBlock"));
	m_IndestructableBlock.setTexture(m_ImageBank->GetImage("IndestructableBlock"));

	m_StatusText.setFont			(m_ImageBank->GetFont());
	m_StatusText.setCharacterSize	(19);
	m_StatusText.setColor			(sf::Color(200,225,250));
	m_StatusText.setPosition		(50, 400);

	m_InfoText.setFont			(m_ImageBank->GetFont());
	m_InfoText.setCharacterSize	(19);
	m_InfoText.setColor			(sf::Color(200,225,250));
	m_InfoText.setPosition		(50, 475);

	m_InfoText.setString("Esc: Exit level editor.\n"
						 "Arrow keys: Select block.\n"
						 "1/2/3: Place basic/strong/indestructable block.\n"
						 "0: Delete block\n"
						 "C: Clear working level.\n"
						 "L: Load currently selected level.\n"
						 "N/M: Select and load previous/next level.\n"
						 "S: Save working level to currently selected spot. This will overwrite the selected level.\n"
						 "A: Append working level to the end of the level file.\n"
						 "Insert: Insert working level before the selected level.\n"
						 "Delete: Permanently delete selected level.");
}
void LevelEditorState::Enter() {
	m_SelectedLevel = 0;
	ReadFromFile();
	LoadSelectedLevel();

	m_SelectedX = 0;
	m_SelectedY = 0;
}

State::StateType LevelEditorState::Update(float delta)
{
	// Input?

	// Exit State
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		return MENUSTATE;
	}

	// Clear working level
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
		m_LevelInProgress.Clear();
		SleepWhileHolding(sf::Keyboard::C);
	}

	// Load currently selected level
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::L)) {
		LoadSelectedLevel();
		SleepWhileHolding(sf::Keyboard::L);
	}

	// Select and load next level
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::M)) {
		if (m_SelectedLevel < m_Levels.size()) {
			m_SelectedLevel++;
		}
		LoadSelectedLevel();
		SleepWhileHolding(sf::Keyboard::M);
	}

	// Select and load previous level
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::N)) {
		if (m_SelectedLevel > 0) {
			m_SelectedLevel--;
		}
		LoadSelectedLevel();
		SleepWhileHolding(sf::Keyboard::N);
	}

	// Save working level at selected position (will overwrite)
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		if (m_SelectedLevel < m_Levels.size()) {
			m_Levels[m_SelectedLevel] = m_LevelInProgress;
		} else {
			m_Levels.push_back(m_LevelInProgress);
		}

		WriteToFile();
		SleepWhileHolding(sf::Keyboard::S);
	}

	// Append working level
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		m_Levels.push_back(m_LevelInProgress);
		m_SelectedLevel = m_Levels.size() - 1;

		WriteToFile();
		SleepWhileHolding(sf::Keyboard::A);
	}

	// Insert working level
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Insert)) {
		if (m_SelectedLevel < m_Levels.size()) {
			m_Levels.insert(m_Levels.begin() + m_SelectedLevel, m_LevelInProgress);
		} else {
			m_Levels.push_back(m_LevelInProgress);
		}

		WriteToFile();
		SleepWhileHolding(sf::Keyboard::Insert);
	}

	// Delete currently selected level
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Delete)) {
		if (m_SelectedLevel < m_Levels.size()) {
			m_Levels.erase(m_Levels.begin() + m_SelectedLevel);

			WriteToFile();

			if (m_SelectedLevel > 0) {
				m_SelectedLevel--;
			}
			LoadSelectedLevel();
		}

		SleepWhileHolding(sf::Keyboard::Delete);
	}

	// Select Block
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		if (m_SelectedX < 9) {
			m_SelectedX++;
		}
		SleepWhileHolding(sf::Keyboard::Right);

	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		if (m_SelectedX > 0) {
			m_SelectedX--;
		}
		SleepWhileHolding(sf::Keyboard::Left);

	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		if (m_SelectedY > 0) {
			m_SelectedY--;
		}
		SleepWhileHolding(sf::Keyboard::Up);

	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		if (m_SelectedY < 9) {
			m_SelectedY++;
		}
		SleepWhileHolding(sf::Keyboard::Down);
	}

	// Add Block
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
		m_LevelInProgress.SetBlock(Level::BASIC, m_SelectedX, m_SelectedY);
		SleepWhileHolding(sf::Keyboard::Num1);

	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
		m_LevelInProgress.SetBlock(Level::STRONG, m_SelectedX, m_SelectedY);
		SleepWhileHolding(sf::Keyboard::Num2);

	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3)) {
		m_LevelInProgress.SetBlock(Level::INDESTRUCTABLE, m_SelectedX, m_SelectedY);
		SleepWhileHolding(sf::Keyboard::Num3);

	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num0)) {
		m_LevelInProgress.SetBlock(Level::NONE, m_SelectedX, m_SelectedY);
		SleepWhileHolding(sf::Keyboard::Num0);

	}

	Draw();

	return LEVELEDITORSTATE;
}


void LevelEditorState::Draw() {
	// Draw blocks of currently selected level
	Level::BlockType type;
	for (int x=0; x<10; x++) {
		for (int y=0; y<10; y++) {
			type = m_LevelInProgress.GetBlock(x, y);

			switch (type) {
			case Level::BASIC:
				m_BasicBlock.setPosition((float)(BLOCK_POSITION_X + BLOCK_WIDTH * x), (float)(BLOCK_POSITION_Y + BLOCK_HEIGHT * y));
				m_Window->draw(m_BasicBlock);
				break;
			case Level::STRONG:
				m_StrongBlock.setPosition((float)(BLOCK_POSITION_X + BLOCK_WIDTH * x), (float)(BLOCK_POSITION_Y + BLOCK_HEIGHT * y));
				m_Window->draw(m_StrongBlock);
				break;
			case Level::INDESTRUCTABLE:
				m_IndestructableBlock.setPosition((float)(BLOCK_POSITION_X + BLOCK_WIDTH * x), (float)(BLOCK_POSITION_Y + BLOCK_HEIGHT * y));
				m_Window->draw(m_IndestructableBlock);
				break;
			}
		}
	}

	// Draw Marker
	m_Marker.setPosition((float)(BLOCK_POSITION_X + BLOCK_WIDTH * m_SelectedX), (float)(BLOCK_POSITION_Y + BLOCK_HEIGHT * m_SelectedY));
	m_Window->draw(m_Marker);

	SetStatusText();
	m_Window->draw(m_StatusText);
	m_Window->draw(m_InfoText);	
}

void LevelEditorState::ReadFromFile() {
	std::ifstream fin("Assets/Levels/levels.bin", std::ios_base::binary);

	if (!fin) {
		LOG("Error: Failed to open Assets/Levels/levels.bin");
	} else {
		int numLevels;
		fin.read((char*)&numLevels, sizeof(int));
		LOG("Number of levels: " << numLevels);


		Level temp;
		for (int i=0; i<numLevels; i++) {
			temp.ReadFromFile(fin);
			m_Levels.push_back(temp);
		}
	}
}
void LevelEditorState::WriteToFile() {
	std::ofstream fout("Assets/Levels/levels.bin", std::ios_base::binary);

	if (!fout) {
		LOG("Error: Failed to write to Assets/Levels/levels.bin");
	} else {
		int numLevels = m_Levels.size();
		fout.write((char*)&numLevels, sizeof(int));
		
		for (int i=0; i<numLevels; i++) {
			m_Levels[i].WriteToFile(fout);
		}

		LOG("Successfully saved Assets/Levels/levels.bin");
	}
}
void LevelEditorState::LoadSelectedLevel() {
	if (m_SelectedLevel < m_Levels.size()) {
		m_LevelInProgress = m_Levels[m_SelectedLevel];
	} else {
		m_LevelInProgress.Clear();
	}
}

void LevelEditorState::SetStatusText() {
	std::string string;

	std::stringstream oss;

	oss << "Selected Level: " << m_SelectedLevel << "\n";
	oss << "Total Number of Levels Saved: " << m_Levels.size();

	m_StatusText.setString(oss.str());
}