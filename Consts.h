#ifndef CONSTS_H
#define CONSTS_H

const int WINDOW_WIDTH = 1200;
const int WINDOW_HEIGHT = 900;

const int BLOCK_POSITION_X	= 95;
const int BLOCK_POSITION_Y	= 60;
const int BLOCK_WIDTH		= 100;
const int BLOCK_HEIGHT		= 30;

const int INDESTRUCTABLE_KEY = 255;

#endif