#include "StdAfx.h"
#include "Player.h"

Player::Player()
{
}

Player::~Player()
{

}

void Player::Init()
{

}

void Player::Update(float delta)
{
	auto it = m_Components.begin();
	while (it != m_Components.end())
	{
		(*it)->Update(delta);
		it++;
	}
}


void Player::Clean()
{

}

bool Player::IsType(const std::string &type)
{
	return type.compare("Player") == 0;
}