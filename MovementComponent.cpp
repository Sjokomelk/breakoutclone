#include "StdAfx.h"
#include "MovementComponent.h"


MovementComponent::MovementComponent()
{

}


MovementComponent::~MovementComponent()
{
}

bool MovementComponent::IsType(const std::string &type)
{
	return type.compare("MovementComponent") == 0;
}

void MovementComponent::Init(GameObject* gameobject)
{
	m_GameObject =  gameobject;

	m_Speed = 5.0f;
	m_bMovement = false;
}

void MovementComponent::Update(float delta)
{
	if (m_GameObject->IsType("Ball")) {
		b2Body* body = m_GameObject->GetComponent<CollisionComponent>("CollisionComponent")->GetBody();
		float xVel = body->GetLinearVelocity().x;
		float yVel = body->GetLinearVelocity().y;
		float minVel = 18.0f;
		float maxVel = 30.0f;

		if (IsMoving()) {

			if (yVel*yVel < minVel*minVel) {
				if (yVel > 0.0f) {
					yVel = minVel;
				} else {
					yVel = -minVel;
				}
			}

			if (xVel*xVel > maxVel*maxVel) {
				if (xVel > 0.0f) {
					xVel = maxVel;
				} else {
					xVel = -maxVel;
				}
			}

			body->SetLinearVelocity(b2Vec2(xVel, yVel));

		}

	}
}

void MovementComponent::Clean()
{

}

void MovementComponent::SetSpeed(float speed)
{
	m_Speed = speed;
}

float MovementComponent::GetSpeed()
{
	return m_Speed;
}

bool MovementComponent::IsMoving()
{
	return m_bMovement;
}

void MovementComponent::SetMovement(bool move)
{
	m_bMovement = move;
}

void MovementComponent::SetDirection(float x, float y)
{
	m_x = x;
	m_y = y;
}
