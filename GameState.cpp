#include "StdAfx.h"
#include "GameState.h"
#include "state.h"


GameState::GameState()
{

}


GameState::~GameState()
{

}

void GameState::Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank)
{
	State::Init(window, imageBank, soundBank);
	
	m_GO_Factory = new GameObjectFactory;

	m_ObjectManager = nullptr;
	m_World = nullptr;
	m_Contacts = nullptr;
	m_WallData = nullptr;
	m_Ball = nullptr;
	m_Player = nullptr;

	 
	m_InfoText.setFont			(m_ImageBank->GetFont());
	m_InfoText.setCharacterSize	(35);
	m_InfoText.setColor			(sf::Color(0, 50, 50));
	m_InfoText.setString		("Press SPACE to start - Use Arrow Keys or AD to move");
	m_InfoText.setPosition		((WINDOW_WIDTH - m_InfoText.getGlobalBounds().width)/2, (WINDOW_HEIGHT - m_InfoText.getGlobalBounds().height)/2);
}
void GameState::Enter() {
	m_ObjectManager = new GameObjectManager;
	b2Vec2 gravity(0.0f, 0.0f);
	m_World = new b2World(gravity);
	m_ObjectManager->Init(m_World);
	m_CurrentLevel = 0;
	m_Contacts = new ContactListener;
	m_World->SetContactListener(m_Contacts);
	m_Contacts->SetGameObjectManager(m_ObjectManager);

	CreateBackground();
	CreateWalls();
	ReadLevels();
	CreateBall();
	CreatePlayer();
	OpenLevel();

	m_GUI.Init(m_Window, m_Player, m_ImageBank, m_Levels.size());
	m_GUI.SetCurrentLevel(m_CurrentLevel);

	m_DrawInfo = true;
}


void GameState::ReadLevels()
{
	std::ifstream fin("Assets/Levels/levels.bin", std::ios_base::binary);

	if (!fin) {
		LOG("Error: Failed to open Assets/Levels/levels.bin");
	} else {
		int numLevels;
		fin.read((char*)&numLevels, sizeof(int));
		LOG("Number of levels: " << numLevels);

		Level temp;
		for (int i=0; i<numLevels; i++) {
			temp.ReadFromFile(fin);
			m_Levels.push_back(temp);
		}
	}
	LOG("Level Size: " << m_Levels.size());
}

void GameState::CreateBall()
{
	PositionComponent* position = new PositionComponent;
	SpriteComponent* sprite = new SpriteComponent;
	CollisionComponent* collision = new CollisionComponent(m_World);
	MovementComponent* movement = new MovementComponent;
	SoundComponent* sound = new SoundComponent;
	InputComponent* inputcomp = new InputComponent;

	GameObject* gameobject = m_GO_Factory->CreateObject(m_GO_Factory->BALL);
	position->Init(gameobject);
	position->SetPosition((WINDOW_WIDTH - 20.0f)/2.0f, 825.0f);
	gameobject->Attach(position);

	sprite->SetImageBank(m_ImageBank);
	sprite->LoadSprite("Ball");
	sprite->Init(gameobject);
	sprite->SetOriginCenter();
	gameobject->Attach(sprite);

	collision->Init(gameobject);
	gameobject->Attach(collision);

	movement->Init(gameobject);
	gameobject->Attach(movement);

	inputcomp->Init(gameobject);
	gameobject->Attach(inputcomp);

	sound->Init(gameobject);
	sound->SetSoundBank(m_SoundBank);

	gameobject->Attach(sound);
	m_Ball = gameobject;
	m_ObjectManager->Insert(gameobject);
}

void GameState::CreateBackground()
{
	PositionComponent* position = new PositionComponent;
	SpriteComponent* sprite = new SpriteComponent;

	GameObject* object = m_GO_Factory->CreateObject(m_GO_Factory->BACKGROUND);
	position->Init(object);
	object->Attach(position);		
	
	sprite->SetImageBank(m_ImageBank);
	sprite->LoadSprite("Background");
	sprite->Init(object);
	object->Attach(sprite);

	m_ObjectManager->Insert(object);

	/*
	NB! Discovered that if we use the position component first and not attaching it, sprite component
	doesn't get the position data, it is uninitialized and the program crashes.

	Because it looks for the game object, then tries to fetch the gameobject component list
	however since it wasn't attached before, like the above it crashed. When Sprite looked for the
	game objects the list was empty and the position component was uninitialized.

	I didn't find a good place to create objects so I put them directly in the gamestate. I know
	this isn't what we wanted. It didn't seem like a good idea to put it directly into the 
	factory. As I've seen the factory pattern is only supposed to return a new type.

	*/

}



void GameState::CreateBlock(sf::Vector2f position, Level::BlockType type)
{
	if (type == Level::NONE) return;
	PositionComponent* pos = new PositionComponent;
	CollisionComponent* collision = new CollisionComponent(m_World);
	SpriteComponent* sprite = new SpriteComponent;
	HealthComponent* health = new HealthComponent;

	GameObject* object = m_GO_Factory->CreateObject(m_GO_Factory->BASICBLOCK);
/* only need the basic blocks, no need to utilize any other classes.*/


	pos->Init(object);
	pos->SetPosition(position);
	object->Attach(pos);

	health->Init(object);
	object->Attach(health);

	sprite->SetImageBank(m_ImageBank);

	sprite->Init(object);
	switch(type)
	{
	case Level::BASIC:
		sprite->LoadSprite("BasicBlock");
		health->SetDestructable(true);
		health->SetHealth(1);
		break;

	case Level::STRONG:
		sprite->LoadSprite("StrongBlock");
		health->SetDestructable(true);
		health->SetHealth(2);
		break;

	case Level::INDESTRUCTABLE:
		sprite->LoadSprite("IndestructableBlock");
		health->SetDestructable(false);
		health->SetHealth(INDESTRUCTABLE_KEY);
		break;

	}
	sprite->SetOriginCenter();
	object->Attach(sprite);

	collision->Init(object);
	object->Attach(collision);
	
	m_ObjectManager->Insert(object);
	m_Blocks.push_back(collision->GetBody());
}

void GameState::CreatePlayer()
{
	
	PositionComponent* pos = new PositionComponent;
	CollisionComponent* collision = new CollisionComponent(m_World);
	SpriteComponent* sprite = new SpriteComponent;
	HealthComponent* health = new HealthComponent;
	InputComponent* inputcomp = new InputComponent;
	MovementComponent* movement = new MovementComponent;

	GameObject* object = m_GO_Factory->CreateObject(m_GO_Factory->PLAYER);

	pos->Init(object);
	pos->SetPosition(1200.0f / 2.0f, 850.0f);
	object->Attach(pos);

	sprite->Init(object);
	sprite->SetImageBank(m_ImageBank);
	sprite->LoadSprite("PlayerPad");
	sprite->SetOriginCenter();
	object->Attach(sprite);

	collision->Init(object);
	object->Attach(collision);

	health->Init(object);
	object->Attach(health);

	movement->Init(object);
	object->Attach(movement);

	inputcomp->Init(object);
	object->Attach(inputcomp);

	m_ObjectManager->Insert(object);
	m_Player = object;
}

State::StateType GameState::Update(float delta)
{
	// Exit State
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		CleanWorld();
		return MENUSTATE;
	}
	
	PositionComponent* position = m_Ball->GetComponent<PositionComponent>("PositionComponent");

	m_ObjectManager->Update(delta);

	if (!m_Ball->GetComponent<MovementComponent>("MovementComponent")->IsMoving()) {
		// Set the ball's horizontal position to be equal to the pad's position
		float padPosX = m_Player->GetComponent<PositionComponent>("PositionComponent")->GetPosition().x;
		float ballPosY = m_Ball->GetComponent<PositionComponent>("PositionComponent")->GetPosition().y;
		m_Ball->GetComponent<PositionComponent>("PositionComponent")->SetPosition(padPosX, ballPosY);
		m_Ball->GetComponent<CollisionComponent>("CollisionComponent")->GetBody()->SetTransform(b2Vec2(padPosX / PPM, ballPosY / PPM), 0.0f);
	}

	//m_World->ClearForces();
	m_World->Step(1.0f/60.0f, 8, 3); // this is for 60frames pr second
	m_ObjectManager->Draw(m_Window);
	if (m_DrawInfo) {
		m_Window->draw(m_InfoText);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
			m_DrawInfo = false;
		}
	}


	/* Works. Think it's easier to just delete the ball body and
	create a new ball if the current health is more than 0.
	*/
	if (m_Blocks.size() > 0)
	{
		unsigned int size = m_Blocks.size();
		for (unsigned int i=0; i<size; i++) {
			HealthComponent* health = static_cast<GameObject*>(m_Blocks[i]->GetUserData())->GetComponent<HealthComponent>("HealthComponent");
			
			if (health->GetHealth() <= 0)
			{
				m_World->DestroyBody(m_Blocks[i]);
				m_Blocks.erase(m_Blocks.begin() + i);
				//LOG("Block erased: " << m_Blocks.size());
				m_GUI.IncreaseScore(100);
				i--;
				size = m_Blocks.size();
			} 
		}
	}
	
	if (position->GetPosition().y >= 1000.0f)
	{
		m_SoundBank->PlayStatic("Death");		
		HealthComponent* hp = m_Player->GetComponent<HealthComponent>("HealthComponent");
		LOG("Decrease health - current:  " << hp->GetHealth());
		hp->DecreaseHealth();
		LOG("Decrease health - after: " << hp->GetHealth());
		int lives = hp->GetHealth();
		LOG("Lives: " << lives);
		m_World->DestroyBody(m_Ball->GetComponent<CollisionComponent>("CollisionComponent")->GetBody());
		m_Ball->Clean();
		m_ObjectManager->Remove(m_Ball);
		CreateBall();
		if (lives <= 0)
		{
			CleanWorld();
			return LOSESTATE;
		}

		if (!m_Ball->GetComponent<MovementComponent>("MovementComponent")->IsMoving()) {
			// Set the ball's horizontal position to be equal to the pad's position
			float padPosX = m_Player->GetComponent<PositionComponent>("PositionComponent")->GetPosition().x;
			float ballPosY = m_Ball->GetComponent<PositionComponent>("PositionComponent")->GetPosition().y;
			m_Ball->GetComponent<PositionComponent>("PositionComponent")->SetPosition(padPosX, ballPosY);
		}
	}
	m_GUI.Update(delta);

	// Check if there are any blocks left if not, open next level
	bool blocksLeft = false;
	for (unsigned int i=0; i<m_ObjectManager->GetSize(); i++) {
		GameObject* temp = m_ObjectManager->GetObject(i);
		if ((temp->IsType("Block")) && 
			(temp->GetComponent<HealthComponent>("HealthComponent")->GetHealth() != INDESTRUCTABLE_KEY)) {
			blocksLeft = true;
		}
	}

	if (!blocksLeft) {
		m_CurrentLevel++;
		if (!OpenLevel()) {
			CleanWorld();
			return WINSTATE;
		}
	}

	return GAMESTATE;
}	

void GameState::CreateWalls()
{
	// Just need some data for the userdata for the walls.
	// Since we're only going to use it for the contact listener I want the 
	// data to be the same as the same sound will play when it hits the wall
	// Therefore just creating a char pointer.
	m_WallData = new Background;
	{
		b2BodyDef NewDef;
		NewDef.type = b2_staticBody;
		NewDef.angle = 0.0f * DEGTORAD;
		b2Body* m_Body = m_World->CreateBody(&NewDef);
		m_Body->SetUserData(m_WallData);

		b2PolygonShape NewRect;
		NewRect.SetAsBox(2.0f / 2.0f / PPM, 1800.0f / 2.0f / PPM);

		b2FixtureDef RectFix;
		RectFix.shape = &NewRect;
		RectFix.density = 1.0f;
		RectFix.friction = 0.0f;
		RectFix.restitution = 0.0f;

		m_Body->CreateFixture(&RectFix);	
		m_Body->SetTransform(b2Vec2(-5.0f / PPM, 900.0f / PPM), 0.0f * DEGTORAD);
		LOG("Left wall: " << m_Body->GetPosition().x << " y: " << m_Body->GetPosition().y);
	}

	{
		b2BodyDef NewDef;
		NewDef.type = b2_staticBody;
		NewDef.angle = 0.0f*DEGTORAD;
		b2Body* m_Body = m_World->CreateBody(&NewDef);
		m_Body->SetUserData(m_WallData);

		b2PolygonShape NewRect;
		NewRect.SetAsBox(2.0f / 2.0f / PPM, 1800.0f / 2.0f / PPM);

		b2FixtureDef RectFix;
		RectFix.shape = &NewRect;
		RectFix.density = 0.1f;
		RectFix.friction = 0.0f;
		RectFix.restitution = 0.00f;

		m_Body->CreateFixture(&RectFix);	
		m_Body->SetTransform(b2Vec2(1200.0f/ PPM, 900.0f / PPM), 0.0f * DEGTORAD);
		LOG("Right wall: " << m_Body->GetPosition().x << " y: " << m_Body->GetPosition().y);
	}
	{
		b2BodyDef NewDef;
		NewDef.type = b2_staticBody;
		//NewDef.position.Set(0.0f / 2.0f / PPM, 2400.0f / 2.0f / PPM);
		NewDef.angle = 0.0f*DEGTORAD;
		b2Body* m_Body = m_World->CreateBody(&NewDef);
		m_Body->SetUserData(m_WallData);

		b2PolygonShape NewRect;
		NewRect.SetAsBox(2400.0f / 2.0f / PPM, 2.0f / 2.0f / PPM);

		b2FixtureDef RectFix;
		RectFix.shape = &NewRect;
		RectFix.density = 0.1f;
		RectFix.friction = 0.0f;
		RectFix.restitution = 0.00f;

		m_Body->CreateFixture(&RectFix);	
		m_Body->SetTransform(b2Vec2(1200.0f / PPM, 0.0f / PPM), 0.0f * DEGTORAD);
		LOG("Roof: " << m_Body->GetPosition().x << " y: " << m_Body->GetPosition().y);
	}

}

void GameState::Clean()
{
	delete m_GO_Factory;
	CleanWorld();
}
void GameState::CleanWorld() {
	if (m_ObjectManager != nullptr) {
		delete m_ObjectManager;
	}

	/* Destroys all bodies in the world that is inserted into the 
	physics world. I'm not sure if this create a memory leak or not. Doesn't
	look like it, however it doesn't hurt to remove the bodies when cleaning up.
	I assume it does that when freeing up the world. 
	*/
	if (m_World != nullptr) {
		b2Body* iter = m_World->GetBodyList();
		while (iter != nullptr)
		{
			b2Body* next = iter->GetNext();
			m_World->DestroyBody(iter);
			iter = next;
		}
		delete m_Contacts;
		delete m_World;
		delete m_WallData;
	}

	m_ObjectManager = nullptr;
	m_World = nullptr;
	m_Contacts = nullptr;
	m_WallData = nullptr;
	m_Ball = nullptr;
	m_Player = nullptr;

	m_Levels.clear();
	m_Blocks.clear();
}

bool GameState::OpenLevel() {
	if (m_Levels.size() <= m_CurrentLevel) return false;

	m_GUI.SetCurrentLevel(m_CurrentLevel);

	// Delete all indestructable locks left from previous level
	unsigned int size = m_ObjectManager->GetSize();
	for (unsigned int i=0; i<size; i++) {
		GameObject* temp = m_ObjectManager->GetObject(i);
		if (temp->IsType("Block")) {
			m_ObjectManager->Remove(temp);

			// Don't count up
			i--;
			size = m_ObjectManager->GetSize();
		}
	}
	

	size = m_Blocks.size();
	for (unsigned int i=0; i<size; i++) {
		m_World->DestroyBody(m_Blocks[i]);
	}
	m_Blocks.clear();

	// Reset ball
	if (m_Ball != nullptr) {
		m_World->DestroyBody(m_Ball->GetComponent<CollisionComponent>("CollisionComponent")->GetBody());
		m_Ball->Clean();
		m_ObjectManager->Remove(m_Ball);
		CreateBall();
	}

	Level::BlockType type;
	for (int x=0; x<10; x++) {
		for (int y=0; y<10; y++) {
			type = m_Levels[m_CurrentLevel].GetBlock(x, y);
			CreateBlock(sf::Vector2f((float)BLOCK_POSITION_X + (float)BLOCK_WIDTH * (float)x + (float)BLOCK_WIDTH * 0.5f, (float)BLOCK_POSITION_Y + (float)BLOCK_HEIGHT * (float)y + (float)BLOCK_HEIGHT * 0.5f), type);
		}
	}

	return true;
}