#pragma once
#include "gameobject.h"
class Background :
	public GameObject
{
public:
	Background();
	~Background();

	void Init();
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type);

private:

};

