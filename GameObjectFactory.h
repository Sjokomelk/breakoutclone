#pragma once

class GameObject;
class Player;
class Ball;
class Block;
class BasicBlock;
class StrongBlock;
class IndestructableBlock;

class GameObjectFactory
{
public:

	enum ObjectType {
		NONE = 0,
		BASICBLOCK,
		STRONGBLOCK,
		INDESTRUCTABLEBLOCK,
		BALL,
		PLAYER,
		BACKGROUND
	};

	GameObjectFactory();
	~GameObjectFactory();

	GameObject* CreateObject(ObjectType object);
	
};

