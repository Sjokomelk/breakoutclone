#pragma once
#include "gameobject.h"

class InterfaceComponent;
class CollisionComponent;
class HealthComponent;
class InputComponent;
class SpriteComponent;
class PositionComponent;
class MovementComponent;

class Player : public GameObject
{
public:
	Player();
	~Player();

	void Init();
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type);

private:
	
};

