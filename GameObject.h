#pragma once

#include "InterfaceComponent.h"

class GameObject
{
public:
	GameObject() {};
	virtual ~GameObject() {
	auto it = m_Components.begin();
	while (it != m_Components.end())
	{
		delete *it;
		++it;
	}
	m_Components.clear();
	};

	virtual void Init() = 0;
	virtual void Update(float delta) = 0;
	virtual void Clean() = 0;
	virtual bool IsType(const std::string &type) = 0;

	void Attach(InterfaceComponent* component)
	{
		m_Components.push_back(component);
	};

	template <class T>
	T* GetComponent(const std::string &type) {
		auto it = m_Components.begin();
		while(it != m_Components.end()) {
			if((*it)->IsType(type)) {
				return static_cast<T*>((*it));
			}
			++it;
		}
		return nullptr;
	};

protected:
	std::vector<InterfaceComponent*> m_Components;

};

