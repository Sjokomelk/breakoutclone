#include "StdAfx.h"
#include "State.h"
#include "MenuState.h"


MenuState::MenuState()
{
}


MenuState::~MenuState()
{
}


void MenuState::Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank)
{
	State::Init(window, imageBank, soundBank);
	InitializeText();
}

void MenuState::Enter() {
	m_SoundBank->StopSounds();
	m_SoundBank->PlayDynamic("EnterMenu");
	m_EscHolding = true;
	m_Selected = GAMESTATE;
}

State::StateType MenuState::Update(float delta)
{
	// Exit State
	if (m_EscHolding) {
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
			m_EscHolding = false;
		}
	} else {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
			return EXITGAME;
		}
	}
	
	// Enter State
	if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) ||
		(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))) {
		if (m_Selected != EXITGAME) {
			m_SoundBank->PlayDynamic("MenuSelect");
		}
		return m_Selected;
	}

	// Select State
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		if (m_Selected == GAMESTATE) {
			m_SoundBank->PlayDynamic("MenuNavigate");
			m_Selected = LEVELEDITORSTATE;
		} else if (m_Selected == LEVELEDITORSTATE) {
			m_SoundBank->PlayDynamic("MenuNavigate");
			m_Selected = EXITGAME;
		}
		SleepWhileHolding(sf::Keyboard::Down);
	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		if (m_Selected == EXITGAME) {
			m_SoundBank->PlayDynamic("MenuNavigate");
			m_Selected = LEVELEDITORSTATE;
		} else if (m_Selected == LEVELEDITORSTATE) {
			m_SoundBank->PlayDynamic("MenuNavigate");
			m_Selected = GAMESTATE;
		}
		SleepWhileHolding(sf::Keyboard::Up);
	}


	Draw();

	return MENUSTATE;
}


void MenuState::Draw() {
	m_Window->draw(m_TitleText);
	m_Window->draw(m_InfoText);

	m_Window->draw(m_PlayText);
	m_Window->draw(m_EditorText);
	m_Window->draw(m_ExitText);

	switch (m_Selected) {
	case GAMESTATE :
		m_Window->draw(m_PlayTextMarked);
		break;
	case LEVELEDITORSTATE :
		m_Window->draw(m_EditorTextMarked);
		break;
	case EXITGAME :
		m_Window->draw(m_ExitTextMarked);
		break;
	}
}
void MenuState::InitializeText() {
	// Initialise the texts:
	m_TitleText.setFont			(m_ImageBank->GetFont());
	m_TitleText.setCharacterSize(75);
	m_TitleText.setColor		(sf::Color(150,50,40));
	m_TitleText.setString		("Breakout");
	m_TitleText.setPosition		((WINDOW_WIDTH - m_TitleText.getGlobalBounds().width)/2, 150);

	m_InfoText.setFont			(m_ImageBank->GetFont());
	m_InfoText.setCharacterSize	(16);
	m_InfoText.setColor			(sf::Color(200,225,250));
	m_InfoText.setString		("Arrow keys: Navigate menu\nSpace/Enter: Select\nEsc: Exit game");
	m_InfoText.setPosition		(WINDOW_WIDTH - m_InfoText.getGlobalBounds().width - 40, WINDOW_HEIGHT - m_InfoText.getGlobalBounds().height - 40);


	m_PlayText.setFont			(m_ImageBank->GetFont());
	m_PlayText.setCharacterSize	(30);
	m_PlayText.setColor			(sf::Color(15,20,100));
	m_PlayText.setString		("Play Game");
	m_PlayText.setPosition		((WINDOW_WIDTH - m_PlayText.getGlobalBounds().width)/2, 350);

	m_EditorText.setFont			(m_ImageBank->GetFont());
	m_EditorText.setCharacterSize	(30);
	m_EditorText.setColor			(sf::Color(15,20,100));
	m_EditorText.setString			("\nOpen Level Editor");
	m_EditorText.setPosition		((WINDOW_WIDTH - m_EditorText.getGlobalBounds().width)/2, 350);

	m_ExitText.setFont			(m_ImageBank->GetFont());
	m_ExitText.setCharacterSize	(30);
	m_ExitText.setColor			(sf::Color(15,20,100));
	m_ExitText.setString		("\n\nExit Game");
	m_ExitText.setPosition		((WINDOW_WIDTH - m_ExitText.getGlobalBounds().width)/2, 350);

	
	m_PlayTextMarked.setFont			(m_ImageBank->GetFont());
	m_PlayTextMarked.setCharacterSize	(30);
	m_PlayTextMarked.setColor			(sf::Color(150,130,40));
	m_PlayTextMarked.setString			("Play Game");
	m_PlayTextMarked.setPosition		((WINDOW_WIDTH - m_PlayText.getGlobalBounds().width)/2, 350);

	m_EditorTextMarked.setFont			(m_ImageBank->GetFont());
	m_EditorTextMarked.setCharacterSize	(30);
	m_EditorTextMarked.setColor			(sf::Color(150,130,40));
	m_EditorTextMarked.setString		("\nOpen Level Editor");
	m_EditorTextMarked.setPosition		((WINDOW_WIDTH - m_EditorText.getGlobalBounds().width)/2, 350);

	m_ExitTextMarked.setFont			(m_ImageBank->GetFont());
	m_ExitTextMarked.setCharacterSize	(30);
	m_ExitTextMarked.setColor			(sf::Color(150,130,40));
	m_ExitTextMarked.setString			("\n\nExit Game");
	m_ExitTextMarked.setPosition		((WINDOW_WIDTH - m_ExitText.getGlobalBounds().width)/2, 350);
}

