#pragma once
#include "interfacecomponent.h"
class GameObject;
class SoundComponent;
class ContactListener;

class CollisionComponent : public InterfaceComponent
{
public:
	CollisionComponent();
	CollisionComponent(b2World* world) : m_World(world) {};
	~CollisionComponent();

	void Init(GameObject* gameobject);
	void Update(float delta);
	void Clean();
	b2Body* GetBody() {return m_Body;}
	
	bool IsType(const std::string &type);
	
private:
	SoundComponent* m_SoundComponent;
	PositionComponent* m_Position;
	SpriteComponent* m_Sprite;
	b2World* m_World;
	b2Body* m_Body;
	b2BodyDef m_BodyDef;
};

