#pragma once
#include "interfacecomponent.h"

class GameObject;
class SoundBank;

class SoundComponent : public InterfaceComponent
{
public:
	SoundComponent();
	~SoundComponent();

	void Init(GameObject* gameobject);
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type);
	SoundBank* GetSoundBank();
	void SetSoundBank(SoundBank* soundbank);

private:
	SoundBank* m_SoundBank;
};

