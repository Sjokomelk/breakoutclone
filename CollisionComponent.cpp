#include "stdafx.h"
#include "CollisionComponent.h"


CollisionComponent::CollisionComponent()
{
}

CollisionComponent::~CollisionComponent()
{
}

void CollisionComponent::Init(GameObject* gameobject)
{
	m_SoundComponent = gameobject->GetComponent<SoundComponent>("SoundComponent");
	m_Position = gameobject->GetComponent<PositionComponent>("PositionComponent");
	m_Sprite = gameobject->GetComponent<SpriteComponent>("SpriteComponent");

	//m_BodyDef.position = b2Vec2(m_Position->GetPosition().x / PPM, m_Position->GetPosition().y / PPM);

	if (gameobject->IsType("Player"))
	{
		Player* player = static_cast<Player*>(gameobject);
		m_BodyDef.type = b2_kinematicBody;
		m_Body = m_World->CreateBody(&m_BodyDef);
		m_Body->SetUserData(player);
		m_BodyDef.position = b2Vec2(m_Position->GetPosition().x / PPM, m_Position->GetPosition().y / PPM);
		m_BodyDef.fixedRotation = false;

		b2PolygonShape Shape;
		Shape.SetAsBox(175.f * 0.5f / PPM, 20.f * 0.5f / PPM);
		b2FixtureDef FixtureDef;
		FixtureDef.density = 0.5f;
		FixtureDef.friction = 0.0f;
		FixtureDef.shape = &Shape;
		m_Body->CreateFixture(&FixtureDef);
		m_Body->SetTransform(b2Vec2(m_Position->GetPosition().x / PPM, m_Position->GetPosition().y / PPM), 0.0f * DEGTORAD);

		LOG("Player Body Created.");
	} else if (gameobject->IsType("Ball"))
	{
		Ball* ball = static_cast<Ball*>(gameobject);
		m_BodyDef.type = b2_dynamicBody;
		m_Body = m_World->CreateBody(&m_BodyDef);
		LOG("Ball created, body pointer: " << m_Body);
		m_Body->SetUserData(ball);
		m_BodyDef.position = b2Vec2(m_Position->GetPosition().x / PPM, m_Position->GetPosition().y / PPM);
		m_BodyDef.fixedRotation = true;
		m_Body->SetLinearDamping(0.0f);
		b2CircleShape Shape;
		Shape.m_radius = 10.0f / PPM;
		b2FixtureDef FixtureDef;
		FixtureDef.density = 1.0f;
		FixtureDef.friction = 0.0f;
		FixtureDef.restitution = 1.0f;
		FixtureDef.shape = &Shape;
		m_Body->CreateFixture(&FixtureDef);
		m_Body->SetTransform(b2Vec2(m_Position->GetPosition().x / PPM, m_Position->GetPosition().y / PPM), 0.0f * DEGTORAD);
		LOG("Ball body created.");
	} else if (gameobject->IsType("Block"))
	{
		m_BodyDef.type = b2_staticBody;
		m_Body = m_World->CreateBody(&m_BodyDef);
		m_Body->SetUserData(gameobject);
		m_BodyDef.position = b2Vec2(m_Position->GetPosition().x / PPM, m_Position->GetPosition().y / PPM);
		b2PolygonShape Shape;
		Shape.SetAsBox(100.0f * 0.5f / PPM, 30.0f * 0.5f / PPM);
		b2FixtureDef FixtureDef;
		FixtureDef.density = 0.0f;
		FixtureDef.friction = 0.0f;
		FixtureDef.restitution = 0.5f;
		FixtureDef.shape = &Shape;
		m_Body->CreateFixture(&FixtureDef);
		m_Body->SetTransform(b2Vec2(m_Position->GetPosition().x / PPM, m_Position->GetPosition().y / PPM), 0.0f * DEGTORAD);
		LOG("Block body created.");
	}
	LOG("Positions multiplied: x: " << m_Body->GetPosition().x * PPM << " y: " << m_Body->GetPosition().y * PPM);
	m_Position->SetPosition(m_Body->GetPosition().x * PPM, m_Body->GetPosition().y * PPM);
		
}

void CollisionComponent::Update(float delta)
{
	m_Position->SetPosition(sf::Vector2f(m_BodyDef.position.x * PPM, m_BodyDef.position.y * PPM));
	m_Sprite->GetSprite()->setRotation(m_Body->GetAngle() * RADTODEG);
}

void CollisionComponent::Clean()
{

}

bool CollisionComponent::IsType(const std::string &type)
{
	if (type.compare("CollisionComponent") == 0)
	{
		return true;
	}
	return false;
}

