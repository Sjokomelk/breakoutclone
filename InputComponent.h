#pragma once
#include "interfacecomponent.h"

class InputComponent : public InterfaceComponent
{
public:
	InputComponent();
	~InputComponent();

	void Init(GameObject* gameobject);
	void Update(float delta);
	void Clean();
	bool IsType(const std::string &type);

private:
	CollisionComponent* m_CollisionComponent;
	PositionComponent* m_PositionComponent;
	SpriteComponent* m_SpriteComponent;
	MovementComponent* m_MovementComponent;
	GameObject* m_GameObject;
	b2Vec2 m_velocity;
	b2Body* m_Body;
	float reporttime;

	float m_SpeedX;
};

