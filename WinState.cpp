#include "StdAfx.h"
#include "State.h"
#include "WinState.h"


WinState::WinState()
{
}


WinState::~WinState()
{
}

void WinState::Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank)
{
	State::Init(window, imageBank, soundBank);
	m_Display.setTexture(m_ImageBank->GetImage("WinState"));
}

void WinState::Enter() {

}

State::StateType WinState::Update(float delta)
{
	// Exit State
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		return MENUSTATE;
	}

	m_Window->draw(m_Display);
	
	return WINSTATE;
}


void WinState::Clean()
{

}


