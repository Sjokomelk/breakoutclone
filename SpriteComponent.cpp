#include "StdAfx.h"
#include "SpriteComponent.h"


SpriteComponent::SpriteComponent()
{
	m_ImageBank = nullptr;
}


SpriteComponent::~SpriteComponent()
{
}

void SpriteComponent::Init(GameObject* gameobject)
{
	m_PositionComponent = gameobject->GetComponent<PositionComponent>("PositionComponent");
	LOG("Initialized GO & got Position component");
	LOG("Position <===> x: " << m_PositionComponent->GetPosition().x << " y: " << m_PositionComponent->GetPosition().y);
	if (gameobject->IsType("Player")) LOG("Player");
	if (gameobject->IsType("Ball")) LOG("Ball");
	if (gameobject->IsType("Block")) LOG("Block");

}

bool SpriteComponent::IsType(const std::string &type)
{
	return type.compare("SpriteComponent") == 0;
}

void SpriteComponent::Update(float delta)
{
	SetPosition();
}

void SpriteComponent::SetImageBank(ImageBank* imagebank) {
	m_ImageBank = imagebank;
}
void SpriteComponent::LoadSprite(std::string path)
{
	if (m_ImageBank != nullptr) {
		m_xSprite = new sf::Sprite();
		m_xSprite->setTexture(m_ImageBank->GetImage(path));
	} else {
		LOG("Error: Imagebank not assigned to spritecomponent");
	}
}

void SpriteComponent::SetOrigin(sf::Vector2f origin)
{
	m_xSprite->setOrigin(origin);
}

void SpriteComponent::SetOrigin(float x, float y)
{
	m_xSprite->setOrigin(x, y);
}

void SpriteComponent::SetOriginCenter()
{
	m_xSprite->setOrigin(m_xSprite->getTexture()->getSize().x * 0.5f, m_xSprite->getTexture()->getSize().y * 0.5f);
}

void SpriteComponent::Clean()
{
	delete m_xSprite;
}

sf::Sprite* SpriteComponent::GetSprite()
{
	return m_xSprite;
}
void SpriteComponent::SetPosition() {
	m_xSprite->setPosition(m_PositionComponent->GetPosition());
}


