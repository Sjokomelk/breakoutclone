#include "StdAfx.h"
#include "PositionComponent.h"


PositionComponent::PositionComponent()
{

}


PositionComponent::~PositionComponent()
{
}

bool PositionComponent::IsType(const std::string &type)
{
	return type.compare("PositionComponent") == 0;
}

void PositionComponent::Init(GameObject* gameobject)
{
	m_Position.x = 0.0f;
	m_Position.y = 0.0f;
	m_GameObject = gameobject;
}

void PositionComponent::Update(float delta)
{

}

sf::Vector2f PositionComponent::GetPosition()
{
	return m_Position;
}

void PositionComponent::SetPosition(sf::Vector2f pPosition)
{
	m_Position = pPosition;
}

void PositionComponent::SetPosition(float x, float y)
{
	m_Position.x = x;
	m_Position.y = y;
}


void PositionComponent::Clean()
{

}


