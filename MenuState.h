#pragma once
#include "state.h"

class MenuState : public State
{
public:
	MenuState();
	~MenuState();

	void Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank);
	void Enter();
	StateType Update(float delta);
	void Clean() {}; // Empty

private:
	void Draw();
	void InitializeText();

	bool m_EscHolding;
	StateType m_Selected;

	sf::Text m_TitleText;
	sf::Text m_InfoText;

	sf::Text m_PlayText;
	sf::Text m_PlayTextMarked;
	sf::Text m_EditorText;
	sf::Text m_EditorTextMarked;
	sf::Text m_ExitText;
	sf::Text m_ExitTextMarked;
};

