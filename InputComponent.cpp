#include "stdafx.h"
#include "InputComponent.h"

InputComponent::InputComponent()
{
}


InputComponent::~InputComponent()
{
}


void InputComponent::Init(GameObject* gameobject)
{
	m_SpeedX = 0.0f;

	m_CollisionComponent = gameobject->GetComponent<CollisionComponent>("CollisionComponent");
	m_PositionComponent = gameobject->GetComponent<PositionComponent>("PositionComponent");
	m_SpriteComponent = gameobject->GetComponent<SpriteComponent>("SpriteComponent");
	m_MovementComponent = gameobject->GetComponent<MovementComponent>("MovementComponent");
	m_Body = m_CollisionComponent->GetBody();
	
	if (gameobject->IsType("Player"))
	{
		LOG("Player Bodypointer " << m_Body);
	}
	if (gameobject->IsType("Ball"))
	{
		LOG("Ball Bodypointer " << m_Body);
	}
	if (gameobject->IsType("Block"))
	{
		LOG("Block Bodypointer " << m_Body);
	}
	
	m_GameObject = gameobject;
	
	m_velocity = b2Vec2(0.0f, 0.0f);
	reporttime = 1.0f;
}

void InputComponent::Update(float delta)
{	
	// Lower speed due to friction
	m_SpeedX -= m_SpeedX * 20.0f * delta;

	/*if (m_SpeedX*m_SpeedX < 5.0f*5.0f) {
		m_SpeedX = 0.0f;
	}*/
	
	if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) && m_GameObject->IsType("Player") && m_Body->GetPosition().x * 30.0f >= 175.0f / 2.0f)
	{
		m_SpeedX -= 500.0f * delta;

	} else if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right) ) && m_GameObject->IsType("Player") && m_Body->GetPosition().x * 30.0f <= 1200.0f - (175.0f / 2))
	{
		m_SpeedX += 500.0f * delta;

	} 
	
	float maxSpeed = 30.0f;
	if (m_SpeedX > maxSpeed) {
		m_SpeedX = maxSpeed;
	} else if (m_SpeedX < -maxSpeed) {
		m_SpeedX = -maxSpeed; 
	} 

	if (m_GameObject->IsType("Player"))
	{
		m_Body->SetTransform(b2Vec2(m_Body->GetPosition().x + m_SpeedX * delta, m_Body->GetPosition().y), 0.0f * DEGTORAD);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && m_GameObject->IsType("Ball") && !m_MovementComponent->IsMoving())
	{
		float x = 0.0f;
		float y = -10.0f;

		while (x*x < 9) {
			x = 10 - (rand()%2000)/100.0f;
		}

		m_Body->SetLinearVelocity(b2Vec2(x,y));
		m_MovementComponent->SetMovement(true);
		
	}

	m_PositionComponent->SetPosition(sf::Vector2f(m_Body->GetPosition().x * PPM, m_Body->GetPosition().y * PPM));
}

void InputComponent::Clean()
{

}

bool InputComponent::IsType(const std::string &type)
{
	return type.compare("InputComponent") == 0;
}