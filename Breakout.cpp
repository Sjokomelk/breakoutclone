// Breakout.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	// Initialize random number generation
	srand(time(0));
	for (int i=0; i<50; i++) {
		rand();
	}

	Game game;
	game.Init();
	game.Update();
	game.Clean();

	return 0;
}


