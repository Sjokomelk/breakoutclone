#pragma once
#include <fstream>
#include <iostream>

class GameObjectFactory;

class GameObjectManager
{
public:
	GameObjectManager();
	~GameObjectManager();

	void Init(b2World* world);
	void Update(float delta);
	void Insert(GameObject* gameobject);
	void Remove(GameObject* gameobject);
	void Draw(sf::RenderWindow* renderer);
	
	unsigned int GetSize() {
		return m_GameObjects.size();
	}
	GameObject* GetObject(unsigned int index) {
		return m_GameObjects[index];
	}

private:
	std::vector<GameObject*> m_GameObjects;
	b2World* m_World;
	std::vector<GameObject*> m_ScheduleForDeletion;

};

