#pragma once
#include "interfacecomponent.h"

class GameObject;

class PositionComponent : public InterfaceComponent
{
public:
	PositionComponent();
	~PositionComponent();

	void Init(GameObject* gameobject);
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type);
	sf::Vector2f GetPosition();
	void SetPosition(sf::Vector2f pPosition);
	void SetPosition(float x, float y);

private:
	sf::Vector2f m_Position;

	GameObject* m_GameObject;

};

