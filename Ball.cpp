#include "StdAfx.h"
#include "Ball.h"


Ball::Ball()
{
}


Ball::~Ball()
{
}

void Ball::Init()
{

}

void Ball::Update(float delta)
{
	auto it = m_Components.begin();
	while (it != m_Components.end())
	{
		(*it)->Update(delta);
		it++;
	}
}

void Ball::Clean()
{
	
}

bool Ball::IsType(const std::string &type)
{
	return type.compare("Ball") == 0;
}
