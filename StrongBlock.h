#pragma once
#include "block.h"
class InterfaceComponent;

class StrongBlock : public GameObject
{
public:
	StrongBlock();
	~StrongBlock();
	
	void Init();
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type) {return type.compare("Block") == 0;}

private:

};

