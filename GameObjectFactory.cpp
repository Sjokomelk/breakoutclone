#include "StdAfx.h"
#include "GameObjectFactory.h"


GameObjectFactory::GameObjectFactory()
{
}


GameObjectFactory::~GameObjectFactory()
{
}

GameObject* GameObjectFactory::CreateObject(ObjectType object)
{
	if (object == BASICBLOCK)
	{
		return new BasicBlock;
	} else if (object == STRONGBLOCK)
	{
		return new StrongBlock;
	} else if (object == INDESTRUCTABLEBLOCK)
	{
		return new IndestructableBlock;
	} else if (object == BALL)
	{
		return new Ball;
	} else if (object == PLAYER)
	{
		return new Player;
	} else if (object == BACKGROUND)
	{
		return new Background;
	}

	return nullptr;
}

