#pragma once
class Level
{
public:
	enum BlockType {
		NONE = 0,
		BASIC = 1,
		STRONG = 2,
		INDESTRUCTABLE = 3
	};
	
	Level();
	~Level();

	void ReadFromFile(std::ifstream& fin);
	void WriteToFile(std::ofstream& fout);

	void SetBlock(BlockType type, int x, int y);
	BlockType GetBlock(int x, int y);

	void Clear();

private:
	// [x][y]
	BlockType m_Blocks[10][10];
};

