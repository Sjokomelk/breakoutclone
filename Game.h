#pragma once
#include "State.h"
#include "Player.h"
#include "BasicBlock.h"

class State;
class MenuState;
class GameState;
class LevelEditorState;
class LoseState;
class WinState;
class Player;
class BasicBlock;
class GameObjectManager;
class GameObject;

class Game
{
public:
	Game();
	~Game();

	void Init();
	void Update();
	void Clean();

private:
	std::vector<State*> m_States;
	State::StateType m_CurrentState;

	sf::RenderWindow* m_Window;
	SoundBank* m_SoundBank;
	ImageBank* m_ImageBank;

	sf::Event m_Event;
	sf::Clock m_Clock;
	float m_fDelta;
		
};

