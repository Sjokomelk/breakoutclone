#pragma once
#include "gameobject.h"
class InterfaceComponent;

class Ball : public GameObject
{
public:
	Ball();
	~Ball();

	void Init();
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type);

private:

};

