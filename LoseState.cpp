#include "StdAfx.h"
#include "state.h"
#include "LoseState.h"


LoseState::LoseState()
{
}


LoseState::~LoseState()
{
}

void LoseState::Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank)
{
	State::Init(window, imageBank, soundBank);
	m_Display.setTexture(m_ImageBank->GetImage("LoseState"));	
}

void LoseState::Enter() {
	m_DisplayTime = 8.0f;
}

State::StateType LoseState::Update(float delta)
{
	// Exit State
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		return MENUSTATE;
	}

	m_Window->draw(m_Display);
	
	if (m_DisplayTime > 0.0f){ 
		m_DisplayTime -= delta;
		return LOSESTATE;
	} else 
	{
		return MENUSTATE;
	}
}

void LoseState::Clean()
{

}
