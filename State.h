#pragma once
class State
{
public:
	enum StateType
	{
		MENUSTATE,
		GAMESTATE,
		LEVELEDITORSTATE,
		LOSESTATE,
		WINSTATE,
		EXITGAME
	};

	State() {};
	~State() {};

	virtual void Init(sf::RenderWindow* window, ImageBank* imageBank, SoundBank* soundBank) {
		m_Window	= window;
		m_ImageBank = imageBank;
		m_SoundBank = soundBank;
	}
	virtual void Enter() = 0;
	virtual StateType Update(float delta) = 0;
	virtual void Clean() = 0;

	void SleepWhileHolding(sf::Keyboard::Key key) {
		sf::Event eventStack;
		while (sf::Keyboard::isKeyPressed(key)) {
			m_Window->pollEvent(eventStack);
			sf::sleep(sf::seconds(0.01f));
		}
	}

protected:
	sf::RenderWindow*	m_Window;
	ImageBank*			m_ImageBank;
	SoundBank*			m_SoundBank;
};

