#include "StdAfx.h"
#include "SoundComponent.h"


SoundComponent::SoundComponent()
{
}


SoundComponent::~SoundComponent()
{
}

void SoundComponent::Init(GameObject* gameobject)
{
	
}

void SoundComponent::Update(float delta)
{

}

void SoundComponent::Clean()
{

}

SoundBank* SoundComponent::GetSoundBank()
{
	return m_SoundBank;
}

void SoundComponent::SetSoundBank(SoundBank* soundbank)
{
	m_SoundBank = soundbank;
}

bool SoundComponent::IsType(const std::string &type)
{
	return true;
}
