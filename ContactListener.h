#pragma once
#include <Box2D\Dynamics\b2WorldCallbacks.h>
class SoundBank;

class ContactListener : public b2ContactListener
{
public:
	ContactListener();
	~ContactListener();

	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);

	void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
	void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);

	void SetGameObjectManager(GameObjectManager* GO_Manager);
	void BallHitPad(GameObject* ball, float padPosX);

private:
	GameObjectManager* m_GO_Manager;
};

