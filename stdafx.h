// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <Box2D/Box2D.h>
#include "GameObject.h"
#include "InterfaceComponent.h"
#include "ImageBank.h"
#include "SoundBank.h"
#include "Game.h"
#include "state.h"
#include "MenuState.h"
#include "GameState.h"
#include "LevelEditorState.h"
#include "Level.h"
#include "LoseState.h"
#include "WinState.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "HealthComponent.h"
#include "MovementComponent.h"
#include "PositionComponent.h"
#include "InputComponent.h"
#include "SoundComponent.h"
#include "GameObjectManager.h"
#include "Player.h"
#include "BasicBlock.h"
#include "StrongBlock.h"
#include "IndestructableBlock.h"
#include "ball.h"
#include "Background.h"
#include "GameObjectFactory.h"
#include "ContactListener.h"
#include "GUI.h"

#include "Consts.h"



#ifdef _DEBUG
#define LOGGING true
#pragma comment(lib, "sfml-graphics-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-network-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "Box2D-d.lib")
#pragma comment(lib, "FreeGLUT-d.lib")
#pragma comment(lib, "GLUI-d.lib")

#endif

#ifndef _DEBUG
#define LOGGING false
#pragma comment(lib, "sfml-graphics.lib")
#pragma comment(lib, "sfml-audio.lib")
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-network.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "Box2D.lib")
#pragma comment(lib, "FreeGLUT.lib")
#pragma comment(lib, "GLUI.lib")
#endif


#define LOG(err_msg) if((LOGGING)) {std::cout << "LOG: " << err_msg << std::endl; }

#define PPM 30.0f
#define DEGTORAD 0.0174532925199432957f
#define RADTODEG 57.295779513082320876f
#define PI 3.14159265
// TODO: reference additional headers your program requires here
