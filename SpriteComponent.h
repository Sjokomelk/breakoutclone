#pragma once
#include "interfacecomponent.h"

class GameObject;
class DisplayManager;
class SpriteManager;

class SpriteComponent : public InterfaceComponent
{
public:
	SpriteComponent();
	~SpriteComponent();

	void Init(GameObject* gameobject);
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type);

	void SetImageBank(ImageBank* imagebank);
	void LoadSprite(std::string path);
	sf::Sprite* GetSprite();
	void SetOrigin(sf::Vector2f origin);
	void SetOrigin(float x, float y);
	void SetOriginCenter();
	void SetPosition();

private:
	GameObject* m_GameObject;
	sf::Sprite* m_xSprite;
	PositionComponent* m_PositionComponent;
	ImageBank* m_ImageBank;
};

