#pragma once
class GUI
{
public:
	GUI();
	~GUI();

	void Init(sf::RenderWindow* window, GameObject* player, ImageBank* imagebank, int numLevels);
	void Update(float delta);
	void Clean();
	void IncreaseScore(int score);
	void SetCurrentLevel(int level);

private:
	sf::RenderWindow* m_Window;
	sf::Text m_Score;
	sf::Text m_Lives;
	sf::Text m_LevelsInfo;
	int m_Points;
	Player* m_Player;
	ImageBank* m_ImageBank;

	int m_NumLevels;
	int m_CurrentLevel;
};

