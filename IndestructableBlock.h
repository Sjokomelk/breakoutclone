#pragma once
#include "block.h"

class InterfaceComponent;

class IndestructableBlock : public GameObject
{
public:
	IndestructableBlock();
	~IndestructableBlock();

	void Init();
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type) {return type.compare("Block") == 0;}

private:

};

