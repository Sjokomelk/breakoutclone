#include "StdAfx.h"
#include "Level.h"


Level::Level() {
	Clear();
}
Level::~Level() {}

void Level::ReadFromFile(std::ifstream& fin) {
	char c = 0;
	for (int x=0; x<10; x++) {
		for (int y=0; y<10; y++) {
			fin.read(&c, sizeof(char));
			
			// Set correct block
			if (c == NONE) {
				m_Blocks[x][y] = NONE;

			} else if (c == BASIC) {
				m_Blocks[x][y] = BASIC;

			} else if (c == STRONG) {
				m_Blocks[x][y] = STRONG;

			} else if (c == INDESTRUCTABLE) {
				m_Blocks[x][y] = INDESTRUCTABLE;
			}
		}
	}
}
void Level::WriteToFile(std::ofstream& fout) {
	char c = 0;
	for (int x=0; x<10; x++) {
		for (int y=0; y<10; y++) {
			c = m_Blocks[x][y];
			fout.write(&c, sizeof(char));
		}
	}
}

void Level::SetBlock(BlockType type, int x, int y) {
	if ((0 <= x) && (x < 10)) {
		m_Blocks[x][y] = type;
	} else {
		LOG("Atempting to assign to a block outside of level range");
	}
}
Level::BlockType Level::GetBlock(int x, int y) {
	if ((0 <= x) && (x < 10)) {
		return m_Blocks[x][y];
	} else {
		LOG("Atempting to fetch a block outside of level range");
	}
	return NONE;
}
void Level::Clear() {
	for (int x=0; x<10; x++) {
		for (int y=0; y<10; y++) {
			m_Blocks[x][y] = NONE;
		}
	}
}