#pragma once
#include "interfacecomponent.h"
class GameObject;

class MovementComponent : public InterfaceComponent
{
public:
	MovementComponent();
	~MovementComponent();

	void Init(GameObject* parent);
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type);

	void SetSpeed(float speed);
	void SetDirection(float x, float y);
	float GetSpeed();
	bool IsMoving();
	void SetMovement(bool move);

private:
	float m_x;
	float m_y;
	float m_Speed;
	bool m_bMovement;
	GameObject* m_GameObject;

	/*
	As it is now, this isn't used at all.
	
	I suppose I should bake this into the Input component. That said I don't 
	want to break stuff now to fix something that really isn't broken, except
	for the usage of this component class.

	Since we're using box2D anyway it handles movement, so there's no real
	need for this. 
	*/

};

