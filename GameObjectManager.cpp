#include "stdafx.h"
#include "GameObjectManager.h"


GameObjectManager::GameObjectManager()
{
}


GameObjectManager::~GameObjectManager()
{

}

void GameObjectManager::Init(b2World* world)
{
	m_World = world;

}

void GameObjectManager::Update(float delta)
{
	for (unsigned int i = 0; i < m_GameObjects.size(); i++)
	{
		m_GameObjects[i]->Update(delta);
		if (m_GameObjects[i]->IsType("Block"))
		{
			HealthComponent* health = m_GameObjects[i]->GetComponent<HealthComponent>("HealthComponent");
			if (health->GetHealth() <= 0)
			{
				m_ScheduleForDeletion.push_back(m_GameObjects[i]);
			}
		}
	}
	if (m_ScheduleForDeletion.size() > 0)
	{
		for (unsigned int i = 0; i < m_ScheduleForDeletion.size(); i++)
		{
			Remove(m_ScheduleForDeletion[i]);
		}
	}
	m_ScheduleForDeletion.clear();
}

void GameObjectManager::Insert(GameObject* gameobject)
{
	m_GameObjects.push_back(gameobject);
}

void GameObjectManager::Remove(GameObject* gameobject)
{
	LOG("Removal");
	auto it = m_GameObjects.begin();
	while (it != m_GameObjects.end())
	{
		if ((*it) == gameobject)
		{
			LOG("Delete: " << gameobject << " iterator: " << (*it));
			delete (*it);
			m_GameObjects.erase(it);
			break;
		}
		it++;
	}
}

void GameObjectManager::Draw(sf::RenderWindow* renderer)
{
	SpriteComponent* sprite = nullptr;
	auto it = m_GameObjects.begin();
	while (it != m_GameObjects.end())
	{
		if ((*it) == nullptr)
		{
			it++;
			continue;
		}
		sprite = (*it)->GetComponent<SpriteComponent>("SpriteComponent");
		if (sprite != nullptr)
		{
			renderer->draw(*sprite->GetSprite());
		}
		it++;
	}

}

