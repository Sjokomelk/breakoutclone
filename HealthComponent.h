#pragma once
#include "interfacecomponent.h"

class GameObject;

class HealthComponent :	public InterfaceComponent
{
public:
	HealthComponent();
	~HealthComponent();

	void Init(GameObject* gameobject);
	void Update(float delta);
	void Clean();

	bool IsType(const std::string &type);

	bool IsDestructable();
	void SetDestructable(bool destructable);
	void SetHealth(int health);
	int GetHealth();

	void DecreaseHealth();

	//HealthComponent& operator--();
	//HealthComponent operator--(int);

private:
	int m_Health;
	bool m_Destructable;
	GameObject* m_GameObject;
};

